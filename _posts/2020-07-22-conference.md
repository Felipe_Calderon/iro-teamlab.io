---
title: "New article (Conference) published!"
layout: post
shortnews: true
icon: newspaper-o
---

New article (Conference) published! [link](https://ieeexplore.ieee.org/document/9203248)
