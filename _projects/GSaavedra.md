---
title: Nonlinearity compensation for multi-band transmission in optical fibre communication systems
subtitle: Proyecto Fondecyt Iniciación

description: |
  The goal of this project is to develop nonlinearity compensation (NLC) strategies in the digital and optical domain to mitigate the detrimental effect of inter-channel stimulated Raman scattering (ISRS) in a multiband transmission environment.

people:
  - gabrielSaavedra

layout: project
last-updated: 2019-11-23
---

Optical fibres are the foremost used communication medium in modern society. They have ubiquitous
presence in telecommunication networks and carry over 95% of the data transmitted around the world.
Although the demonstrated information throughput achieved using optical fibre links has increased year
after year, it has been acknowledged the capacity of optical fibres is limited. Single-mode, silica based
optical fibres are a nonlinear medium, and fibre nonlinearities limit the amount of power that can be
launched into a transmission link, a thus its information throughput. To increase the throughput of optical
communication systems the use of multiple transmission bands/windows within the low-loss region has
been proposed. This solution, however, has the drawback of enhancing nonlinear interactions in the
transmission fibre. In addition, as the transmission bandwidth is increased, the power transfer from low to
high wavelength channels within the optical signal further modifies the nonlinear behaviour of the system.
It has been noted that inter-channel Raman scattering can reduced the information throughput between
10-40% for bandwidths of 15 THz. Lately, research has focused on finding ways to mitigate or compensate
for the effects of fibre nonlinearities, with digital and optical signal processing techniques used for this
purpose. To make the most of multi-band transmission system nonlinearity compensation methods tailored
to work in such an environment will be necessary.

The goal of this project is to develop nonlinearity compensation (NLC) strategies in the digital and optical
domain to mitigate the detrimental effect of inter-channel stimulated Raman scattering (ISRS) in a multi-
band transmission environment. For this purpose, modified digital signal processing algorithms, such as
digital backpropagation and Volterra series equalisation, capable of working under an ISRS modified signal
power profiles will be developed. In addition, the algorithms will be able to adapt to changes of the
transmission parameters such as signal power and bandwidth. In the optical domain, the use of the optical
phase conjugation technique to equalise the power transfer experienced by the transmitted channels will
be studied and optimised. Finally, a study of the implementation complexity of the proposed methods will
be performed to provide some useful insight on which method will require less computational or optical
resources for future implementation.

To accomplish the goal of this proposal a numerical simulator of optical transmission systems will be
developed to evaluate the nonlinear propagation of multi-band signals in optical fibres. For this purpose,
numerical algorithms and models will be produced to describe fibre propagation in such a wideband
transmission regime. The simulator will be able to account for all the nonlinear effects present in a multi-
band transmission environment. Digital and optical NLC methods capable of compensating the detrimental
effects imposed by the use of large optical bandwidths will be developed, implemented in MatLab and
subsequently used to process the simulated signals. Additionally, experimental data will be obtained in
collaboration with the optical networks group from UCL to assess the performance of the NLC methods
developed during the project. Finally, the implementation challenges for optical communication systems to
adopt the developed techniques will be theoretically studied to find which solutions is more suitable with
state-of-the-art components. The described solutions are expected to recover the performance lost due to
the ISRS effect, in addition to compensate for nonlinearities arising from the Kerr effect. Therefore, an
increase in the overall throughput is expected from both solutions of at least 10%.